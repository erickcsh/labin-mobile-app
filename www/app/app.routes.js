(function() {
  'use strict';

  angular
    .module('labinMobileApp')
    .config(routesConfiguration);

  /* @ngInject */
  function routesConfiguration($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/tab/home');
  }

})();
